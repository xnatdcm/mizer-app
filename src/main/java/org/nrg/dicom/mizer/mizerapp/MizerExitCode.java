package org.nrg.dicom.mizer.mizerapp;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.boot.ExitCodeGenerator;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_")
public class MizerExitCode extends RuntimeException implements ExitCodeGenerator {
    public MizerExitCode(final int exitCode, final String message) {
        _exitCode = exitCode;
        _message = message;
    }

    public static ExitCodeGenerator getExitCodeGenerator(final int exitCode, final String message) {
        return new MizerExitCode(exitCode, message);
    }

    private final int    _exitCode;
    private final String _message;
}
