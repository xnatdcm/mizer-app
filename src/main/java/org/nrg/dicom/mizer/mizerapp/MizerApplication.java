package org.nrg.dicom.mizer.mizerapp;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import javax.annotation.Nullable;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class, DataSourceAutoConfiguration.class, WebMvcAutoConfiguration.class})
@ComponentScan({"org.nrg.dcm.edit.mizer", "org.nrg.dicom.dicomedit.mizer", "org.nrg.dicom.mizer.service.impl"})
@Slf4j
public class MizerApplication implements ApplicationRunner, ApplicationContextAware {
    public static void main(final String[] arguments) {
        final SpringApplication app = new SpringApplication(MizerApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(arguments);
    }

    @Override
    public void run(final ApplicationArguments arguments) throws IOException, MizerException {
        if (arguments.getOptionNames().contains("help")) {
            SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(0, display("help.base")));
        }

        if (!arguments.getOptionNames().containsAll(Arrays.asList("manifest", "scripts", "destination"))) {
            SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(1, display(Arrays.asList("help.required", "help.base"))));
        }

        final List<String> manifests       = arguments.getOptionValues("manifest");
        final List<String> scriptManifests = arguments.getOptionValues("scripts");
        final List<String> destinations    = arguments.getOptionValues("destination");
        if (manifests.size() == 0 || manifests.size() > 1 || scriptManifests.size() == 0 || scriptManifests.size() > 1 || destinations.size() == 0 || destinations.size() > 1) {
            SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(1, display(Arrays.asList("help.oneandonlyone", "help.base"))));
        }

        final File manifestFile = getFile(manifests.get(0), "help.nomanifest");
        final File scriptFile   = getFile(scriptManifests.get(0), "help.noscripts");
        final File destination  = Paths.get(destinations.get(0)).toFile();
        if (destination.isFile()) {
            SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(2, display("help.destinationisfile", destination.getPath())));
        }
        if (!destination.exists()) {
            System.out.print("The destination folder " + destination.getPath() + " does not exist. Should I try to create it? ");
            final Scanner scanner = new Scanner(System.in);
            final String  input   = scanner.nextLine();
            if (StringUtils.equalsIgnoreCase(input, "n")) {
                SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(2, display("help.notcreated", destination.getPath())));
            }
            final boolean success = destination.mkdirs();
            if (!success) {
                SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(2, display("help.createfailed", destination.getPath())));
            }
        }

        System.out.println(display("app.started", manifestFile.getPath(), scriptFile.getPath(), destination.getPath()));
        final List<MizerContext> contexts = getMizerContexts(scriptFile);
        final Path target = destination.toPath();

        for (final File file : getManifestFiles(manifestFile)) {
            final File working = Files.copy(file.toPath(), target.resolve(file.getName())).toFile();
            _service.anonymize(working, contexts);
        }
    }

    private List<File> getManifestFiles(final File manifest) throws FileNotFoundException {
        final List<File> files = new ArrayList<>();
        try (final Scanner scanner = new Scanner(manifest)) {
            while (scanner.hasNextLine()) {
                final File file = Paths.get(scanner.nextLine()).toFile();
                if (!file.exists()) {
                    throw new FileNotFoundException("The specified DICOM file " + file.getPath() + " couldn't be found.");
                }
                files.add(file);
            }
        }
        return files;
    }

    private List<MizerContext> getMizerContexts(final File scriptFile) throws FileNotFoundException {
        final List<MizerContext> contexts = new ArrayList<>();
        try (final Scanner scanner = new Scanner(scriptFile)) {
            while (scanner.hasNextLine()) {
                final File script = Paths.get(scanner.nextLine()).toFile();
                if (!script.exists()) {
                    throw new FileNotFoundException("The specified script file " + script.getPath() + " couldn't be found.");
                }
                try (final InputStream inputStream = new FileInputStream(script)) {
                    contexts.add(new MizerContextWithScript(inputStream));
                } catch (IOException | MizerException e) {
                    final String message = "The script " + script.getPath() + " couldn't be read";
                    log.error(message, e);
                    throw new MizerExitCode(4, message + "\n" + e.getMessage());
                }
            }
        }
        return contexts;
    }

    @Override
    public void setApplicationContext(final ApplicationContext context) throws BeansException {
        _context = context;
    }

    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Autowired
    public void setMizerService(final MizerService service) {
        _service = service;
    }

    private File getFile(final String filespec, final String failureMessage) {
        final File file = Paths.get(filespec).toFile();
        if (!file.exists()) {
            SpringApplication.exit(_context, MizerExitCode.getExitCodeGenerator(2, display(failureMessage, file.getPath())));
        }
        return file;
    }

    private String display(final List<String> messages) {
        return display(Maps.toMap(messages, new Function<String, List<Object>>() {
            @Override
            public List<Object> apply(@Nullable final String input) {
                return Arrays.asList(ArrayUtils.EMPTY_OBJECT_ARRAY);
            }
        }));
    }

    private String display(final Map<String, List<Object>> messages) {
        final StringBuilder builder = new StringBuilder();
        for (final String messageKey : messages.keySet()) {
            builder.append(display(messageKey, messages.get(messageKey))).append("\n");
        }
        return builder.toString();
    }

    private String display(final String messageKey, final Object... arguments) {
        return messageSource().getMessage(messageKey, arguments, Locale.getDefault());
    }

    private static ApplicationContext _context;

    private MizerService _service;
}
