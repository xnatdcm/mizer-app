# Mizer Application #

The Mizer application is bone stupid at this point.

## Building the Mizer Application

Build the mizer application with the following command:

```bash
./gradlew clean shadowJar
```

This command creates a runnable jar in **build/libs/mizer-app-_version_-shadow.jar**.

## Running the Mizer Application

You need three things before you can run the Mizer application:

* A file containing a list of the input DICOM files
* A file containing a list of files with anonymization scripts
* The path to an output folder

Run the mizer application with the following command:

```bash
java -jar mizer-app-0.0.1-SNAPSHOT-shadow.jar --manifest=manifest.txt --scripts=scripts.txt --destination=output
```

All of the DICOM files listed in **manifest.txt** will be processed through the scripts listed in **scripts.txt**
and placed in the folder **output**. The original source DICOM files are left unchanged.

